import { Component , Input} from '@angular/core';

@Component({
  selector:'app-card',
  templateUrl:'./cards.component.html',
  styleUrls:['./cards.component.scss']
})



export class CardsComponent{
  @Input() user;

  isMarked=false;

  onClick(){
    // console.log("clicked");
    this.isMarked=true;
  }
}
