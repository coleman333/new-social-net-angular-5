import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  providers: [UserService]

})
export class HomePageComponent implements OnInit {

  users = [];
  searchStr="";
  users2=[];

  constructor(private userService:UserService) {

  }

  ngOnInit() {
    // this.users = this.userService.users;

    this.userService.getUsers().subscribe(users=>{   //console.log(users)
      this.users = users;
    });

  }

  onChange2(){
    this.userService.getUsers2().subscribe(users2=>{ 
      console.log(users2);
      this.users2= users2;
    })
  }


}
