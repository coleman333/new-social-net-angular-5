import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CardsComponent } from './cards/cards.component';
import { CopyCardsComponent } from './copyCards/copyCards.component';
import {HoverDirective} from "./hover.directive";
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from "@angular/forms";
import {SearchPipe} from "./search.pipe";
import { HomePageComponent } from './home-page/home-page.component';
import { SetupPageComponent } from './setup-page/setup-page.component';
import {RouterModule} from "@angular/router";
// import {UserService} from "./user.service";


const routes = [
  {  path:'',component:HomePageComponent},
  {  path:'setup',component:SetupPageComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    CardsComponent,
    CopyCardsComponent,
    HoverDirective,
    SearchPipe,
    HomePageComponent,
    SetupPageComponent,
    // UserService
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
