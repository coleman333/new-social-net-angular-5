let Passport = require('passport');
let moment = require('moment');
let multer = require('multer');
let usersModel = require('../models/userModel');
// let tasksGroupModel = require('../models/tasksGroup');
var path = require('path');
const async = require('async');
const _ = require('lodash');
const util = require('util');


module.exports.upload = multer({
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      var absolutePath = path.join(__dirname, '../../images/');
      cb(null, absolutePath);
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
  })
});

module.exports.getUserAngular4 = function () {

};


module.exports.createUser = async function (req, res, next) {
  const user = req.body;

  try {
    const newUser = await usersModel.create(user);
    res.json(newUser);
  } catch (ex) {
    console.error(ex);
    return next(ex);
  }
};

module.exports.getUser = function (req,res,next) {


};

// module.exports.updateGroupTaskNewAvatar = function (req, res, next) {
//   console.log(req.body);
//   let groupTask = JSON.parse(req.body.group); //{_id,title,....}
//
//   groupTask.id_user = req.user._id;
//   try {
//     groupTask.tasksAvatar = '/images/' + req.file.filename;
//   } catch (ex) {
//   }
//   tasksGroupModel.update({'_id': groupTask._id}, {$set: groupTask}, function (err, group) {
//     if (err) {
//       console.error(err);
//       return next(err);
//     }
//     res.status(200);
//     res.json(group);
//   });
// };
//
// group.id_user = req.user._id;
// group.ready_group_or_not = true;
// tasksGroupModel.update({'_id': req.body._id}, {$set: group}, function (err, group) {
//   if (err) {
//     console.error(err);
//     return next(err);
//   }
//   res.status(200);
//   res.json(group);
// });
// }
// ;
//
