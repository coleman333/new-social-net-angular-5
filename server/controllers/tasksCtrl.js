let Passport = require('passport');
let moment = require('moment');
let multer = require('multer');
let tasksModel = require('../models/tasksModel');
let tasksGroupModel = require('../models/tasksGroup');
var path = require('path');
const async = require('async');
const _ = require('lodash');

module.exports.upload = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            var absolutePath = path.join(__dirname, '../../images/');
            cb(null, absolutePath);
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        }
    })
});

module.exports.createTask = function (req, res, next) {
    let task = req.body;
    console.log(req.body);
    task.id_user = req.user._id;
    tasksModel.create(task, function (err, task) {
        if (err) {
            console.error(err);
            return next(err);
        }
        else {
            res.statusCode = 200;
            res.json(task);
        }
    })
};

module.exports.updateTask = function (req, res, next) {
    let task = req.body;
    console.log(req.body);
    task.id_user = req.user._id;

    tasksModel.update({'_id': req.body._id}, {$set: task}, function (err, task) {      //?
        if (err) {
            console.error(err);
            return next(err);
        }

        res.status(200);
        res.json(task);
    });
};

module.exports.createGroupTask = function (req, res, next) {

    let groupTask = JSON.parse(req.body.groupTask);

    groupTask.id_user = req.user._id;
    try {
        groupTask.tasksAvatar = '/images/' + req.file.filename;
    } catch (ex) {
    }
    tasksGroupModel.create(groupTask, function (err, groups) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.status(200);
        res.json(groups);
    });
};

module.exports.updateGroupTaskNewAvatar = function (req, res, next) {
    console.log(req.body);
    let groupTask = JSON.parse(req.body.group); //{_id,title,....}

    groupTask.id_user = req.user._id;
    try {
        groupTask.tasksAvatar = '/images/' + req.file.filename;
    } catch (ex) {
    }
    tasksGroupModel.update({'_id': groupTask._id}, {$set: groupTask}, function (err, group) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.status(200);
        res.json(group);
    });
};

module.exports.updateGroupTaskSameAvatar = function (req, res, next) {
    let group = req.body;
    console.log(req.body);
    group.id_user = req.user._id;

    tasksGroupModel.update({'_id': req.body._id}, {$set: group}, function (err, group) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.status(200);
        res.json(group);
    });
};

module.exports.showCommonTasks = function (req, res, next) {
    var selector = {};
    selector.id_user = req.user._id;
    if (!_.isUndefined(req.body.ready_or_not)) {
        selector.ready_or_not = req.body.ready_or_not;
    }
    // console.log(req.body.pageNumber);
    // if(req.body.pageNumber == undefined){req.body.pageNumber =1}
    if (req.body.endDate && req.body.ready_or_not == false) {
        let startDate = moment().valueOf();
        let endDate = moment(req.body.endDate).valueOf();

        selector.$or = [
            {
                dateTask: {$lte: startDate},
                deadLine: {$gte: endDate}
            },
            {
                dateTask: {
                    $gte: startDate,
                    $lte: endDate
                }
            },
            {
                deadLine: {
                    $gte: startDate,
                    $lte: endDate
                }
            }
        ];
    }
    if (req.body.urgentTask == true && req.body.ready_or_not == false) {
        let startDate = moment().valueOf();
        let endDate = moment().add(3, 'days').valueOf();
        selector.$or = [
            {deadLine: {$lte: startDate}},
            {deadLine: {$lte: endDate, $gte: startDate}}
        ];
    }
    if (req.body.endDate && req.body.ready_or_not == true) {
        let startDate = moment().valueOf();
        let endDate = moment(req.body.endDate).valueOf();

        selector.$or = [
            {
                dateTask: {
                    $lte: startDate,
                    $gte: endDate
                }
            },
            {
                deadLine: {
                    $lte: startDate,
                    $gte: endDate
                }
            },
            // {
            //     deadLine: {$gte: startDate},
            //     dateTask: {$lte: endDate}
            // },


        ];
    }
    if (req.body.uncertain) {
        selector.deadLine = null;
        // console.log(req.body.endDate);
    }
    if (req.body.searchTasks) {
        selector.title = req.body.title;

    }
    if (req.body.groupId) {
        selector.groupTasks = req.body.groupId;
    }

    async.waterfall([
            function (callback) {
                tasksModel.find(selector)
                    .limit(req.body.limit || 10)
                    .skip(req.body.offset || 0)
                    .exec(function (err, tasks) {
                        if (err) {
                            return callback(err);
                        }
                        callback(null, tasks)
                        // console.log(tasks);
                    })
            },
            function (tasks, callback) {
                tasksModel.count(selector, function (err, count) {
                    if (err) {
                        return callback(err);
                    }
                    callback(null, tasks, count)
                });
            }
        ],
        function (err, tasks, count) {
            if (err) {
                console.log(err);
                return next(err);
            }
            res.statusCode = 200;
            res.json({tasks, count});
        }
    )
};

module.exports.showGroups = function (req, res, next) {
    var selector = {};
    selector.id_user = req.user._id;
    // selector.ready_group_or_not = false;
    tasksGroupModel.find(selector, function (err, tasksGroup) {
        if (err) {
            console.log(err);
            return next(err);
        }
        else {
            res.statusCode = 200;
            res.json(tasksGroup);
        }
    })
};

module.exports.deleteGroup = function (req, res, next) {
    var selector = {};
    selector._id = req.body._id;

    tasksGroupModel.remove(selector, function (err) {
        if (err) {
            console.log(err);
            return next(err);
        }
        else {
            res.statusCode = 200;
            // res.json({_id:req.body._id});
            res.end();
        }
    });
    var selectorTasks = {};
    selectorTasks.groupTasks = req.body._id;
    tasksModel.remove(selectorTasks, function (err) {
        if (err) {
            console.log(err);
            return next(err);
        }
        else {
            res.statusCode = 200;
            // res.json({_id:req.body._id});
            res.end();
        }
    })

};

module.exports.deleteTask = function (req, res, next) {
    var selector = {};
    console.log(145, req.body._id);
    selector._id = req.body._id;
    tasksModel.remove(selector, function (err) {
        if (err) {
            console.log(err);
            return next(err);
        }
        else {
            res.statusCode = 200;
            res.json({_id: req.body._id});
        }
    })
};

module.exports.doneGroup = function (req, res, next) {
    let group = req.body;
    console.log(req.body);

    var Task = {};
    Task.groupTasks = req.body._id;
    Task.ready_or_not = true;
    tasksModel.update({'groupTasks': Task.groupTasks}, {$set: Task}, function (err, Task) {
        if (err) {
            console.log(err);
            return next(err);
        }
        else {
            res.statusCode = 200;
            // res.json(Task);
        }
    });

    group.id_user = req.user._id;
    group.ready_group_or_not = true;
    tasksGroupModel.update({'_id': req.body._id}, {$set: group}, function (err, group) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.status(200);
        res.json(group);
    });
};

module.exports.doneTask = function (req, res, next) {
    let task = req.body;
    console.log(req.body);
    task.id_user = req.user._id;
    task.ready_or_not = true;
    tasksModel.update({'_id': req.body._id}, {$set: task}, function (err, task) {
        if (err) {
            console.error(err);
            return next(err);
        }
        res.status(200);
        res.json(task);
    });
};