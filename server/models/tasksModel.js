const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let tasksSchema = new Schema({
    title : {type:String,required:true},
    deadLine:{type:Date,required:false},
    dateTask:{type:Date,required:false},                            //  (the date the best for this task)
    periodHours :{type:Number,required:false},             //(the time the best for this task)
    periodMinutes:{type:Number,required:false},             // (the time period for this task)
    // picture_done_or_not	:{type:String,required:false},     //(if the task active picture transperant ,if done move to the done)
    ready_or_not:{type:Boolean,default:false},
    // avatar:{type:String,default:false},
    groupTasks:{type:Schema.ObjectId,ref: 'TaskGroup',required:false},                     //(urgent,common,special,ent.)
    subGroupTasks:{type:String,required:false},
    id_user:{type:Schema.ObjectId,ref: 'User',required:true}
    
});

module.exports = mongoose.model('Task',tasksSchema);