const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// one more changes here
let tasksGroupSchema = new Schema({
    groupTitle : {type:String,required:true},
    GroupDeadLine:{type:Date,required:false},
    // dateTask:{type:Date,required:false},                            //  (the date the best for this task)
    // periodHours :{type:Number,required:false},             //(the time the best for this task)
    // periodMinutes:{type:Number,required:false},             // (the time period for this task)
    avatar:{type:String,required:false},                    //avatar of the group
    tasksAvatar :{type:String,required:false},                 //(urgent,common,special,ent.)
    ready_group_or_not:{type:Boolean,default:false},
    subGroupTasks:[{subGroupTitle:{type:String,required:false}}],
    id_user:{type:Schema.ObjectId,ref: 'User',required:true}
    
});
// new branch
module.exports = mongoose.model('TaskGroup',tasksGroupSchema);
// changes in master
