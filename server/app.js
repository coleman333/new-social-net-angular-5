var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const webpack = require('webpack');
var passport = require('passport');
var session = require('express-session');
var index = require('./routes/index');
var fs = require('fs');
let userCtrl = require('./controllers/usersCtrl');
let userRouter = require('./routes/users');
let tasksRouter = require('./routes/tasks');
const passportConfig = require('./PassportConfig');
passportConfig(passport);
var MongoStore = require('connect-mongo')(session);         //для сохранения в mongo а не в ОЗУ

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/testAngular4Mongo');                 //db on loc
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('we\'re connected!');
});

var app = express();

const isDev = process.env.NODE_ENV === 'development';

if (isDev) {
    const config = require('../webpack.config.js');
    let compiler = webpack(config);
    app.use(require('webpack-dev-middleware')(compiler, {
        noInfo: true,
        publicPath: config.output.publicPath
    }));
    app.use(require('webpack-hot-middleware')(compiler));
}

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.use(session({                                           //для подключения сессий к node
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: {secure: false},                              //false для http ,true для https
    store: new MongoStore({mongooseConnection: db})         //для сохранения в mongo а не в ОЗУ
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/images/', function (req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    return res.json({message: 'Access denied'});
}, express.static(path.join(__dirname, '../images')));

app.use('/api/users/', userRouter);
app.use('/api/tasks/', tasksRouter);
// catch 404 and forward to error handler

// app.get('/images/:filename', function (req, res, next) {
//     //на запрос на сервер
//     let readStream = fs.createReadStream(path.join(__dirname, `../images/${req.params.filename}`));
//     readStream.on('error', function (err) {
//         next(err)
//         /*handle error*/
//     });
//     //записываем в readStream ссылку на поток по пути к файлу через модуль fs.createReadStream
//     readStream.pipe(res);                   //записываем этот же поток в ответ к пользователю в res
// });

app.use('*', function (req, res, next) {
    fs.readFile(path.join(__dirname, '../public/index.html'), function (err, indexHtml) {
        res.end(indexHtml);
    })
});

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json({type: 'error', message: res.locals.message, err: res.locals.error});
});

module.exports = app;
