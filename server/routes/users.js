var express = require('express');
var router = express.Router();
// var path = require('path');
var UserController = require('../controllers/usersCtrl');
var testRequest = require('../controllers/testRequest');


router.post('/registration', UserController.upload.single('avatar'), UserController.registration);
router.post('/login', UserController.login);
router.post('/logout', UserController.logout);
router.post('/isUser', UserController.isUser);
router.post('/', testRequest.createUser);
router.get('/getUser',testRequest.getUser);
// router.get('/search/:id',function(req,res,next){
//     console.dir(req.body)
//     console.dir(req.params)
//     console.dir(req.query)
//     res.end()
// });

module.exports = router;


